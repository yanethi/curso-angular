import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';

// Tenemos un decorador @NgModule que agrega metadatos a la clase inmediatamente siguiente (en este caso, AppModule).
//Nuestro decorador @NgModule tiene cuatro claves: declaraciones, importaciones, proveedores y bootstrap.
@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

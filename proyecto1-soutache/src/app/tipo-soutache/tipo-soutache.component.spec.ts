import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoSoutacheComponent } from './tipo-soutache.component';

describe('TipoSoutacheComponent', () => {
  let component: TipoSoutacheComponent;
  let fixture: ComponentFixture<TipoSoutacheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoSoutacheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoSoutacheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

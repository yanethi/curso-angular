import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { TipoSoutache } from './../models/tipo-soutache.models';

@Component({
  selector: 'app-tipo-soutache',
  templateUrl: './tipo-soutache.component.html',
  styleUrls: ['./tipo-soutache.component.css']
})
export class TipoSoutacheComponent implements OnInit {

  @Input() tipo:TipoSoutache;
  @HostBinding('attr.class') cssClass='col-md-4';

  constructor() {
    //this.nombre='nombre por defecto';
  }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaSoutacheComponent } from './lista-soutache.component';

describe('ListaSoutacheComponent', () => {
  let component: ListaSoutacheComponent;
  let fixture: ComponentFixture<ListaSoutacheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaSoutacheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaSoutacheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { TipoSoutache } from './../models/tipo-soutache.models';

@Component({
  selector: 'app-lista-soutache',
  templateUrl: './lista-soutache.component.html',
  styleUrls: ['./lista-soutache.component.css']
})
export class ListaSoutacheComponent implements OnInit {

  tipos:TipoSoutache[];

  constructor() {
    this.tipos=[];
  }

  ngOnInit() {
  }

  agregar(nombre:string, descripcion:string):boolean {
    this.tipos.push(new TipoSoutache(nombre, descripcion));
    //console.log(this.tipos);
      return false;
  }

}

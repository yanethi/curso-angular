export class TipoSoutache {
  nombre:string;
  descripcion:string;

  constructor(n:string, d:string){
    this.nombre=n;
    this.descripcion=d;
  }
}

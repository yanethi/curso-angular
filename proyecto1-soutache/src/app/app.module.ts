import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TipoSoutacheComponent } from './tipo-soutache/tipo-soutache.component';
import { ListaSoutacheComponent } from './lista-soutache/lista-soutache.component';

@NgModule({
  declarations: [
    AppComponent,
    TipoSoutacheComponent,
    ListaSoutacheComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
